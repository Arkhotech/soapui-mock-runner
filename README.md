# Soap UI

Una imagen que permite correr mock services especificados en un proyecto SoapUI.

- Imagen base: java-alpine <https://bitbucket.org/gui77aume/java-alpine>
- SoapUI 5.3

## Usage

La imagen levanta todos los mocks services configurados en el proyecto SoapUI que se encontra en /project.xml
Ese proyecto tiene que ser compartido desde el host:

docker run -p 8080:8080 -v /soapui/myProject.xml:/project.xml -d soapui-mock-service
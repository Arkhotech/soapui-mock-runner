FROM gui77aume/java-alpine

ADD SoapUI-5.3.0-linux-bin.tar.gz /

ENTRYPOINT /SoapUI-5.3.0/bin/mockservicerunner.sh /project.xml